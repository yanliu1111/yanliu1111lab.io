---
title: "Markdown Cheat Sheet"
date: 2022-05-19T17:12:45-06:00
draft: false
---

# Title

## subtitle

### small title

paragraph 1 has plain text

paragraph 2 has **bold text**

paragraph 3 is *italic*

pragraphn 4 is ***bold and italic***

following line of python code `print("hello world!")` is simple

link to this local file: [link](http://localhost:1313/posts/2022/000-markdown/)

a image: ![yes](/image.png)

image that you can click [![dog](https://www.akc.org/wp-content/uploads/2017/11/Golden-Retriever-Puppy.jpg)](https://www.duckduckgo.com)

> blockquotes goes here  
> continue

---

## subtitle

1. item 1
2. item 2
3. item 3

- item 1
    - items
        - item inside
- item 2
- item 3


block of codes:
```
shopt -s nullglob
for g in `find /sys/kernel/iommu_groups/* -maxdepth 0 -type d | sort -V`; do
    echo "IOMMU Group ${g##*/}:"
    for d in $g/devices/*; do
        echo -e "\t$(lspci -nns ${d##*/})"
    done;
done;

```